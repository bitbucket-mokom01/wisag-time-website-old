export * from './login.service';
import { LoginService } from './login.service';
export * from './timeAuto.service';
import { TimeAutoService } from './timeAuto.service';
export * from './timeManuell.service';
import { TimeManuellService } from './timeManuell.service';
export * from './timeOverview.service';
import { TimeOverviewService } from './timeOverview.service';
export const APIS = [LoginService, TimeAutoService, TimeManuellService, TimeOverviewService];
