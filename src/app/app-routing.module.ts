import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
  {
    path: "login",
    loadChildren: () =>
      import("./pages/login/login.module").then(m => m.LoginModule)
  },

  {
    path: "company",
    loadChildren: () =>
      import("./pages/company/company.module").then(m => m.CompanyModule),
    canActivate: []
  },

  {
    path: "time",
    loadChildren: () =>
      import("./pages/time/time.module").then(m => m.TimeModule),
    canActivate: [AuthGuard]
  },
  // {
  //   path: "passwordchange",
  //   loadChildren: () =>
  //     import("./pages/changePassword/changePassword.module").then(m => m.ChangePasswordModule)
  // },
  // {
  //   path: "dashboard",
  //   loadChildren: () =>
  //     import("./pages/dashboard/dashboard.module").then(m => m.DashboardModule),
  //   canActivate: [AuthGuard]
  // },
  
  {
    path: "**",
    redirectTo: 'time', pathMatch: 'full'
  },
  {
    path: "",
    redirectTo: 'time', pathMatch: 'full'
    // loadChildren: () =>
    //   import("./pages/time/time.module").then(m => m.TimeModule),
    // canActivate: [AuthGuard]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false, scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
