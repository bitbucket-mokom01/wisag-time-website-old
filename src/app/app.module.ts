import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PagesModule } from "./pages/pages.module";
import { SharedModule } from "./shared/shared.module";

import { ApiModule, Configuration, ConfigurationParameters } from "../app/api";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AuthGuard } from './guards/auth.guard';
import { GlobalService } from './core/global/global.service';
import { RouterModule } from '@angular/router';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {};
  return new Configuration(params);
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    SharedModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: 'de'
    }),

    HttpClientModule,
    
    ApiModule.forRoot(apiConfigFactory)
  ],
  providers: [
    AuthGuard,
    GlobalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
