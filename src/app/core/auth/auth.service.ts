import { Injectable, Optional } from "@angular/core";
import {
  LoginService,
  LoginRequest,
  LoginResponse,
} from "../../api/";

import { Observable, Observer } from "rxjs";
import { LocalStorageService } from "../storage/localStorage.service";
import * as jwt_decode from "jwt-decode";
import { Router } from "@angular/router";
import { Configuration } from "../../../../src/app/api";
import { GlobalService } from "../global/global.service";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private isAuthorized: boolean;
  private user: any; // real user after a successfull login
  private isDummy: boolean = false;
  private company: any;

  constructor(
    private loginService: LoginService,
    // private passwordResetService: PasswordResetService,    
    
    // private userService: UserService,
    private router: Router,
    private configuration: Configuration,
    private localStorageService: LocalStorageService,
    private globalService: GlobalService
  ) {
    this.user = this.localStorageService.get("wisag-time-user") as any;
    this.company = this.localStorageService.get("wisag-time-company") as any;

    if (this.user) {
      this.user = JSON.parse(this.user) as object;
      this.isAuthorized = true;
      this.configuration.accessToken = this.user.token;
    } else {
      this.isAuthorized = false;
    }
  }

  /**
   * login
   * executes a login request based on the given username and password
   * @param username
   * @param password
   * @returns Observable LoginResponse
   */
  public login(username: string, password: string): Observable<any> {
    const loginRequest: LoginRequest = {
      username: username,
      password: password
    };
    return Observable.create((observer: Observer<any>) => {
      if(username === "admin@nix.de" && password === "Admin") {
        this.isAuthorized = true;
        this.isDummy = true;
        this.saveUserInLocalStorage({token: 'dummy-token', userid: 'dummy-userid'});
        
        observer.next({token: 'dummy-token', userid: 'dummy-userid'});
        observer.complete();
      } else {
        observer.error({error: "dummy login fail", username: username, password: password});
      }
      // @TODO backend logic here
      // this.loginService.getJwToken(loginRequest).subscribe(
      //   (res: LoginResponse) => {
      //     //success
      //     this.saveUserInLocalStorage(res); 
      //     this.isAuthorized = true;
      //     this.configuration.accessToken = res.token;

          
      //     observer.next(res);
      //     observer.complete();
      //   },
      //   err => {
      //     this.isAuthorized = false;
      //     observer.error(err);
      //   }
      // );
    });
  }

  public setCompany(company): boolean {
    this.company = company;

    return this.saveCompanyInLocalStorage(this.company);
  }

  public getCompany(): any {
    return this.company;
  }

  /**
   * forgotPassword
   * executes a createPasswordReset request based on the given username
   * @param username
   * @returns Observable PasswordResetPostResponse
   */
  // public forgotPassword(username: string): Observable<any> {
  //   const forgotPasswordRequest: PasswordResetPostRequest = {
  //     username: username,
  //     baseDomain: window.location.origin
  //   };

  //   return Observable.create((observer: Observer<any>) => {
  //     this.passwordResetService.createPasswordReset(forgotPasswordRequest).subscribe(
  //       (res: PasswordResetPostResponse) => {
  //         if(!res || !res.success) {
  //           observer.error(res);
  //         }

  //         //success
  //         observer.next(res);
  //         observer.complete();
  //       },
  //       err => {
  //         observer.error(err);
  //       }
  //     );
  //   });
  // }

  // /**
  //  * forgotPassword
  //  * executes a createPasswordReset request based on the given username
  //  * @param username
  //  * @returns Observable PasswordResetPostResponse
  //  */
  // public forgotPasswordSet(password: string, hash: string): Observable<any> {
  //   const forgotPasswordSetRequest: PasswordResetSetPostRequest = {
  //     password: password,
  //     hash: hash
  //   };

  //   return Observable.create((observer: Observer<any>) => {
  //     this.passwordResetService
  //       .passwordResetSet(hash, forgotPasswordSetRequest)
  //       .subscribe(
  //         (res: PasswordResetPostResponse) => {
  //           if (!res || !res.success) {
  //             observer.error(res);
  //           }

  //           //success
  //           observer.next(res);
  //           observer.complete();
  //         },
  //         err => {
  //           observer.error(err);
  //         }
  //       );
  //   });
  // }

  /**
   * logout
   * removes the wisag-time-user from localStorage, reset the isAuthorized variable and redirects to the login
   */
  public logout(): void {
    this.localStorageService.remove("wisag-time-user");
    this.localStorageService.remove("wisag-time-company");
    this.isAuthorized = false;
    this.company = null;
    this.isDummy = false;
    this.router.navigateByUrl("/login");
  }

  /**
   * getUser
   * returns the currently loged in user
   * @return {Object} this.user
   */

  public getUser() {
    return this.user;
  }

  /**
   * isUserLogedIn
   * returns a boolean if a user is currently loged in
   * @returns {boolean} this.isAuthorized
   */
  public isUserLogedIn(): boolean {
    return this.isAuthorized;
  }


  /**
   * isDummyLogin
   * returns a boolean if a user is currently loged in
   * @returns {boolean} this.isAuthorized
   */
  public isDummyLogin(): boolean {
    return this.isDummy;
  }


  /**
   * saveUserInLocalStorage
   * saves the given user in the localStorage
   * @param res
   * @return {Boolean}
   */
  private saveUserInLocalStorage(res): any {
    if (!res.token) {
      return false;
    }

    // var decodedToken = jwt_decode(res.token);
    this.localStorageService.set(
      "wisag-time-user",
      JSON.stringify({
        token: res.token,
        userid: res.userid
      })
    );

    return true;
  }

  /**
   * saveCompanyInLocalStorage
   * saves the given company in the localStorage
   * @param company {string}
   * @return {Boolean}
   */
  private saveCompanyInLocalStorage(company): boolean {
    if (!company) {
      return false;
    }

    this.localStorageService.set(
      "wisag-time-company",
      JSON.stringify({
        company: company
      })
    );

    return true;
  }
}
