import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
    
  constructor(private router: Router) { }

  /**
   * goTo
   *
   * @param url
   */
  goTo(url: string): void {
    this.router.navigateByUrl(url);
    window.scroll(0,0);
  }
}
