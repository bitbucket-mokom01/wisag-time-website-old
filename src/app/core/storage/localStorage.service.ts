import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class LocalStorageService {

    constructor() { }

    /**
     * set
     * sets the given key with the given value in localstorage
     * @param key 
     * @param value 
     */
    public set(key: string, value: string):void {
        localStorage.setItem(key, value);
    }

    /**
     * get
     * returns a localstorage item based on the given key   
     * @param key 
     */
    public get(key: string):string {
        return localStorage.getItem(key);
    }

    /**
     * remove
     * removes a single localStorage entry by key
     */
    public remove(key: string): void {
        localStorage.removeItem(key);
    }

    /**
     * clearAll
     * clears the whole localStorage
     */
    public clearAll():void {
        localStorage.clear();
    }
}