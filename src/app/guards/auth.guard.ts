import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log("authGuard called");
    if (localStorage.getItem("wisag-time-user")) {
      if (localStorage.getItem("wisag-time-company")) {        
        return true;
      }
      
      this.router.navigate(["/company"]);
      return false;
    }

    this.router.navigate(["/login"]);
    return false;
  }
}
