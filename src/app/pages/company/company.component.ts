import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  public company: string = "";
  public readonly: boolean;

  constructor(public router: Router, public authService: AuthService) { }

  ngOnInit(): void {
    let company = this.authService.getCompany();
    if(company) {
      this.router.navigateByUrl("time/auto");
    }
    if(!this.authService.isUserLogedIn()) {
      this.router.navigateByUrl("/login");
    }
  }

  submit() {
    this.authService.setCompany(this.company);

    this.router.navigateByUrl("time/auto");
  }
}
