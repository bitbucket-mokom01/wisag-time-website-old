import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CompanyRoutingModule } from "./company.routing";

// import { MatInputModule } from "@angular/material/input";
import { CompanyComponent } from "./company.component";
import { SharedModule } from "src/app/shared/shared.module";
import { AuthService } from "src/app/core/auth/auth.service";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [CompanyComponent],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    // MatInputModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: []
})
export class CompanyModule {}
