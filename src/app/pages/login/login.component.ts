import { Component, OnInit } from "@angular/core";
import { AuthService } from '../../core/auth/auth.service';
import { Router } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  
  username: string;
  password: string;


  loginFailed: boolean = false;

  isLoading = false;
  loginRequired = true;

  loginForm = new FormGroup({
    username: new FormControl(this.username),
    password: new FormControl(this.password)
  });


  constructor(public authService: AuthService, private router: Router) {

  }

  ngOnInit() {
    if(this.authService.isUserLogedIn()) {
      this.router.navigateByUrl("/company")
    }
    // maybe skip intant to time auto if only one company exists?
  }
  /**
   * login
   *
   * @param event
   */
  public login(): any {
    if(this.isLoading || !this.loginForm.valid) {
      return false;
    }
    this.loginFailed = false;
    this.isLoading = true;

    this.authService.login(this.username, this.password)
      .subscribe(
        res => {
          this.router.navigateByUrl("/company");
          // maybe skip intant to time auto if only one company exists?
        },
        err => {
          alert('Diese Kombination aus Benutzername und Passwort ist nicht bekannt.')
        },
      )
      .add(() => {
        this.loginFailed = true;        
        this.isLoading = false;
      })
    ;
  }

}
