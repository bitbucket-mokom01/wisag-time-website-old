import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoginModule } from "./login/login.module";
import { CompanyModule } from "./company/company.module";
import { TimeModule } from "./time/time.module";
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    LoginModule,
    TimeModule,
    CompanyModule  
  ],
  exports: [],
  declarations: []
})
export class PagesModule {}
