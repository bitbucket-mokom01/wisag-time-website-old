import { Component, OnInit } from "@angular/core";
import { TimeService } from '../time.service';
import { Router } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";
import { GlobalService } from 'src/app/core/global/global.service';
import { TranslateService } from '@ngx-translate/core';

enum TimeAutoState {
  not_started = 'not_started',
  started = 'started',
  stopped = 'stopped',
  paused = 'paused'
}

@Component({
  selector: "app-time-auto",
  templateUrl: "./time-auto.component.html",
  styleUrls: ["./time-auto.component.scss"]
})

export class TimeAutoComponent implements OnInit {
  private today: Date;
  public isCostCenterModeSelect: boolean;
  public isKMStateOpen: boolean;
  public timeAutoState: TimeAutoState;

  constructor(private translateService: TranslateService, public globalService: GlobalService, private timeService: TimeService, private router: Router) {
    this.today = new Date();
    this.isCostCenterModeSelect = true;
    this.isKMStateOpen = false;
    this.timeAutoState = TimeAutoState.not_started;
  }

  ngOnInit() {

  }

  /**
   * getTodayText
   * return a specific date string
   * expl: Heute, Mi 19. August 2020
   *
   * @return string
   */
   getTodayText(): string {
    const options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' };
    // let x = await this.translateService.get('Heute, ').toPromise();
    // console.log(x);
    let lang = this.translateService.currentLang + '-' + this.translateService.currentLang.toLocaleUpperCase();


    return this.today.toLocaleDateString(lang, options).replace(".", "").replace(", ", " ");
  }

}
