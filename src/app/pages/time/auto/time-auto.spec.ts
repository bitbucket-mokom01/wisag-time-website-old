import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TimeAutoComponent } from "./time-auto.component";

describe("TimeAutoComponent", () => {
  let component: TimeAutoComponent;
  let fixture: ComponentFixture<TimeAutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimeAutoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
