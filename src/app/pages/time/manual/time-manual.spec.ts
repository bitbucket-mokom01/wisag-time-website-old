import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TimeManualComponent } from "./time-manual.component";

describe("TimeManualComponent", () => {
  let component: TimeManualComponent;
  let fixture: ComponentFixture<TimeManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TimeManualComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
