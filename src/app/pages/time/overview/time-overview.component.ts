import { Component, OnInit } from "@angular/core";
import { TimeService } from '../time.service';
import { Router } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";
import { GlobalService } from 'src/app/core/global/global.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: "app-time-overview",
  templateUrl: "./time-overview.component.html",
  styleUrls: ["./time-overview.component.scss"]
})
export class TimeOverviewComponent implements OnInit {
  private today: Date;

  constructor(private translateService: TranslateService, public globalService: GlobalService, private timeService: TimeService, private router: Router) {
    this.today = new Date();
  }

  ngOnInit() {

  }

   /**
   * getTodayText
   * return a specific date string
   * expl: Heute, Mi 19. August 2020
   *
   * @return string
   */
  getTodayText(): string {
    const options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' };

    let lang = this.translateService.currentLang + '-' + this.translateService.currentLang.toLocaleUpperCase();

    return this.today.toLocaleDateString(lang, options).replace(".", "").replace(", ", " ");
  }




}
