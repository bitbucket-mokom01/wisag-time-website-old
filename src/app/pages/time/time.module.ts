import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TimeRoutingModule } from "./time.routing";

// import { MatInputModule } from "@angular/material/input";

import { SharedModule } from "src/app/shared/shared.module";
import { AuthService } from "src/app/core/auth/auth.service";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TimeAutoComponent } from './auto/time-auto.component';
import { TimeOverviewComponent } from './overview/time-overview.component';
import { TimeManualComponent } from './manual/time-manual.component';
import { TimeEditComponent } from './edit/time-edit.component';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [TimeAutoComponent, TimeOverviewComponent, TimeManualComponent, TimeEditComponent],
  imports: [
    RouterModule,
    CommonModule,
    TimeRoutingModule,
    // MatInputModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: []
})
export class TimeModule {}
