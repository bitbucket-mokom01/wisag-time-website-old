import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TimeAutoComponent } from './auto/time-auto.component';
import { TimeManualComponent } from './manual/time-manual.component';
import { TimeOverviewComponent } from './overview/time-overview.component';
import { TimeEditComponent } from './edit/time-edit.component';


const routes: Routes = [
  { path: "auto", component:  TimeAutoComponent},
  { path: "manual", component:  TimeManualComponent}, 
  { path: "overview", component:  TimeOverviewComponent}, 
  { path: "edit", component:  TimeEditComponent}, 
  { path: '', redirectTo: 'auto', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimeRoutingModule {}
