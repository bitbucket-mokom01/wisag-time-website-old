import { Component, OnInit, OnDestroy, HostListener, ElementRef } from "@angular/core";
import { AuthService } from "../../../core/auth/auth.service";
// import { UserService, UserResponse } from "../../../api";
import { GlobalService } from "../../../core/global/global.service";
import { Subscription } from "rxjs";
import { LocalStorageService } from "../../../core/storage/localStorage.service";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit, OnDestroy {
  userFullName: string;
  userLastLogin: string;
  subscriptionUserFullName: Subscription;
  subscriptionUserLastLogin: Subscription;

  isLngMenuOpen: boolean = false;
  public currentLanguage: string;

  constructor(
    public authService: AuthService,
    // private userService: UserService,
    private globalService: GlobalService,
    private localStorageService: LocalStorageService,
    private eRef: ElementRef,
    private translateService: TranslateService
  ) {
    // this.subscriptionUserFullName = this.globalService
    //   .getCurrentUserFullName()
    //   .subscribe(currentUserFullName => {
    //     this.userFullName = currentUserFullName;
    //   });

    // this.subscriptionUserLastLogin = this.globalService
    //   .getCurrentUserLastLogin()
    //   .subscribe(currentUserLastLogin => {
    //     this.userLastLogin = currentUserLastLogin;
    //   });
  }

  @HostListener('document:click', ['$event'])
  @HostListener('document:touchstart', ['$event'])
  handleOutsideClick(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.isLngMenuOpen = false;
    }
  }

  ngOnInit() {
    this.translateService.use('de');

    // init language from localStorage?

    this.currentLanguage = this.translateService.currentLang;
    
  }

  setLanguage(lang) {
    this.translateService.use(lang);
    this.currentLanguage = lang;
    this.isLngMenuOpen = false;
  }

  ngOnDestroy() {
    // this.subscriptionUserFullName.unsubscribe();
    // this.subscriptionUserLastLogin.unsubscribe();
  }
}
