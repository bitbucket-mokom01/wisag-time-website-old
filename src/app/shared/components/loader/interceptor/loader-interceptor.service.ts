import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import { Observable } from "rxjs";
import { tap, delay } from "rxjs/operators";
import { LoaderService } from "../service/loader.service";

export const InterceptorSkipLoadingHeader = "X-Skip-LoadingInterceptor";

@Injectable({
  providedIn: "root"
})
export class LoaderInterceptorService implements HttpInterceptor {
  constructor(private loaderService: LoaderService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (req.headers.has(InterceptorSkipLoadingHeader)) {
      const headers = req.headers.delete(InterceptorSkipLoadingHeader);
      return next.handle(req.clone({ headers }));
    }

    this.showLoader();

    return next.handle(req).pipe(
      delay(350),
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            this.onEnd();
          }
        },
        (err: any) => {
          this.onEnd();
        }
      )
    );
  }

  private onEnd(): void {
    this.hideLoader();
  }

  private showLoader(): void {
    this.loaderService.show();
  }

  private hideLoader(): void {
    this.loaderService.hide();
  }
}
