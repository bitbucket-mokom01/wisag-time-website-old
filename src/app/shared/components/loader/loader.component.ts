import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";

import { LoaderService } from "./service/loader.service";
import { LoaderState } from "./loader.model";
import { trigger, transition, style, animate } from "@angular/animations";
@Component({
  selector: "app-loader",
  templateUrl: "./loader.component.html",
  styleUrls: ["./loader.component.css"],
  animations: [
    trigger("fadeOut", [
      transition(':leave', [
        style({ opacity: 1 }),
        animate('0.5s', style({ opacity: 0 })),
      ]),
    ])
  ]
})
export class LoaderComponent implements OnInit, OnDestroy {
  show = false;

  private loaderSubscription: Subscription;

  constructor(private loaderService: LoaderService) {}

  ngOnInit() {
    this.loaderSubscription = this.loaderService.loaderState.subscribe((state: LoaderState) => {
      this.show = state.show;
    });
  }

  ngOnDestroy() {
    this.loaderSubscription.unsubscribe();
  }
}
