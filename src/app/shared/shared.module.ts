import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
// import { ButtonComponent } from "./components/button/button.component";
import { LoaderComponent } from "./components/loader/loader.component";
import { HeaderComponent } from "./components/header/header.component";
import { FooterComponent } from "./components/footer/footer.component";
import { RouterModule } from "@angular/router";
import { TranslateModule } from '@ngx-translate/core';
// import { SidebarComponent } from "./components/sidebar/sidebar.component";
// import { MomentModule } from "ngx-moment";

@NgModule({
  imports: [CommonModule, RouterModule, TranslateModule ],
  declarations: [
    //ButtonComponent,
    LoaderComponent,
    HeaderComponent,
    FooterComponent,
    //SidebarComponent
  ],
  exports: [
    //ButtonComponent,
    LoaderComponent,
    HeaderComponent,
    //SidebarComponent,
    FooterComponent
  ]
})
export class SharedModule {}
